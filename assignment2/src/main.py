
import da
import sys
import time
import lamutex
import ramutex
import ratoken

def main():
    p = int(sys.argv[1])
    r = int(sys.argv[2])
    n = int(sys.argv[3])
    s = int(sys.argv[4])
    m = int(sys.argv[5])
    for i in range(n):
        print('lamutex')
        lamutex.main(p, r)
        print('ramutex')
        ramutex.main(p, r)
        print('ratoken')
        ratoken.main(p, r)
        time.sleep(2)
    print('performance testing')
    count = 0
    print('#######################')
    step = (int((r / s)) - 1)
    if (step <= 0):
        step = 1
    for i in range(1, r, step):
        count += 1
        sum_lamutex_1 = 0
        sum_ramutex_1 = 0
        sum_ratoken_1 = 0
        sum_lamutex_2 = 0
        sum_ramutex_2 = 0
        sum_ratoken_2 = 0
        for j in range(m):
            t0 = time.clock()
            t1 = time.time()
            lamutex.main(p, i)
            sum_lamutex_2 += (time.time() - t1)
            sum_lamutex_1 += (time.clock() - t0)
            t0 = time.clock()
            t1 = time.time()
            ramutex.main(p, i)
            sum_ramutex_2 += (time.time() - t1)
            sum_ramutex_1 += (time.clock() - t0)
            t0 = time.clock()
            t1 = time.time()
            ratoken.main(p, i)
            sum_ratoken_2 += (time.time() - t1)
            sum_ratoken_1 += (time.clock() - t0)
        if (count > s):
            break
        print('p->', p, 'r->', i)
        print('cpu times')
        print('lamutex avg time->', (sum_lamutex_1 / m), '\nramutex avg time->', (sum_ramutex_1 / m), '\nratoken avg time->', (sum_ratoken_1 / m))
        print('wall clock times')
        print('lamutex avg time->', (sum_lamutex_2 / m), '\nramutex avg time->', (sum_ramutex_2 / m), '\nratoken avg time->', (sum_ratoken_2 / m))
        print('#######################')
    count = 0
    step = (int((p / s)) - 1)
    if (step <= 0):
        step = 1
    for i in range(1, p, step):
        count += 1
        sum_lamutex_1 = 0
        sum_ramutex_1 = 0
        sum_ratoken_1 = 0
        sum_lamutex_2 = 0
        sum_ramutex_2 = 0
        sum_ratoken_2 = 0
        for j in range(m):
            t0 = time.clock()
            t1 = time.time()
            lamutex.main(i, r)
            sum_lamutex_2 += (time.time() - t1)
            sum_lamutex_1 += (time.clock() - t0)
            t0 = time.clock()
            t1 = time.time()
            ramutex.main(i, r)
            sum_ramutex_2 += (time.time() - t1)
            sum_ramutex_1 += (time.clock() - t0)
            t0 = time.clock()
            t1 = time.time()
            ratoken.main(i, r)
            sum_ratoken_2 += (time.time() - t1)
            sum_ratoken_1 += (time.clock() - t0)
        if (count > s):
            break
        print('p->', i, 'r->', r)
        print('cpu times')
        print('lamutex avg time->', (sum_lamutex_1 / m), '\nramutex avg time->', (sum_ramutex_1 / m), '\nratoken avg time->', (sum_ratoken_1 / m))
        print('wall clock times')
        print('lamutex avg time->', (sum_lamutex_2 / m), '\nramutex avg time->', (sum_ramutex_2 / m), '\nratoken avg time->', (sum_ratoken_2 / m))
        print('#######################')
