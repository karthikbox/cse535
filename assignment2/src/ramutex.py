
import da
PatternExpr_0 = da.pat.TuplePattern([da.pat.ConstantPattern('cs in'), da.pat.FreePattern('c2'), da.pat.FreePattern('p')])
PatternExpr_1 = da.pat.TuplePattern([da.pat.ConstantPattern('cs out'), da.pat.FreePattern('c2'), da.pat.FreePattern('p')])
PatternExpr_3 = da.pat.TuplePattern([da.pat.ConstantPattern('Done'), da.pat.BoundPattern('_BoundPattern10_')])
PatternExpr_4 = da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.FreePattern(None), da.pat.FreePattern(None)]), da.pat.TuplePattern([da.pat.ConstantPattern('Done'), da.pat.BoundPattern('_BoundPattern20_')])])
PatternExpr_6 = da.pat.TuplePattern([da.pat.ConstantPattern('Done'), da.pat.BoundPattern('_BoundPattern23_')])
PatternExpr_8 = da.pat.TuplePattern([da.pat.ConstantPattern('Request'), da.pat.FreePattern('timestamp')])
PatternExpr_9 = da.pat.FreePattern('source')
PatternExpr_10 = da.pat.TuplePattern([da.pat.ConstantPattern('Reply'), da.pat.FreePattern('c1')])
PatternExpr_11 = da.pat.FreePattern('source')
PatternExpr_7 = da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.FreePattern(None), da.pat.FreePattern(None)]), da.pat.TuplePattern([da.pat.ConstantPattern('Done'), da.pat.BoundPattern('_BoundPattern33_')])])
import sys

class M(da.DistProcess):

    def __init__(self, parent, initq, channel, props):
        super().__init__(parent, initq, channel, props)
        self._MReceivedEvent_2 = []
        self._events.extend([da.pat.EventPattern(da.pat.ReceivedEvent, '_MReceivedEvent_0', PatternExpr_0, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._M_handler_0]), da.pat.EventPattern(da.pat.ReceivedEvent, '_MReceivedEvent_1', PatternExpr_1, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._M_handler_1]), da.pat.EventPattern(da.pat.ReceivedEvent, '_MReceivedEvent_2', PatternExpr_3, sources=None, destinations=None, timestamps=None, record_history=True, handlers=[])])

    def setup(self, s, nrequest):
        self.s = s
        self.nrequest = nrequest
        self.f = list()

    def _da_run_internal(self):
        p = None

        def UniversalOpExpr_0():
            nonlocal p
            for p in self.s:
                if (not PatternExpr_4.match_iter(self._MReceivedEvent_2, _BoundPattern20_=p)):
                    return False
            return True
        _st_label_25 = 0
        while (_st_label_25 == 0):
            _st_label_25 += 1
            if UniversalOpExpr_0():
                _st_label_25 += 1
            else:
                super()._label('_st_label_25', block=True)
                _st_label_25 -= 1
        self.solve()

    def solve(self):
        isSafe = True
        for i in range(1, len(self.f)):
            if (self.f[i][0] == 'cs out'):
                if ((not (self.f[(i - 1)][0] == 'cs in')) or (not (self.f[i][2] == self.f[(i - 1)][2]))):
                    isSafe = False
                    break
            elif (self.f[i][0] == 'cs in'):
                if ((not (self.f[(i - 1)][0] == 'cs out')) or (self.f[i][2] == self.f[(i - 1)][2])):
                    isSafe = False
                    break
        if isSafe:
            self.output('safety check PASSED')
        else:
            self.output('safety check FAILED')

    def _M_handler_0(self, c2, p):
        self.f.append(('cs in', c2, p))
    _M_handler_0._labels = None
    _M_handler_0._notlabels = None

    def _M_handler_1(self, c2, p):
        self.f.append(('cs out', c2, p))
    _M_handler_1._labels = None
    _M_handler_1._notlabels = None

class P(da.DistProcess):

    def __init__(self, parent, initq, channel, props):
        super().__init__(parent, initq, channel, props)
        self._PReceivedEvent_0 = []
        self._events.extend([da.pat.EventPattern(da.pat.ReceivedEvent, '_PReceivedEvent_0', PatternExpr_6, sources=None, destinations=None, timestamps=None, record_history=True, handlers=[]), da.pat.EventPattern(da.pat.ReceivedEvent, '_PReceivedEvent_1', PatternExpr_8, sources=[PatternExpr_9], destinations=None, timestamps=None, record_history=None, handlers=[self._P_handler_2]), da.pat.EventPattern(da.pat.ReceivedEvent, '_PReceivedEvent_2', PatternExpr_10, sources=[PatternExpr_11], destinations=None, timestamps=None, record_history=None, handlers=[self._P_handler_3])])

    def setup(self, ps, nrounds, m):
        self.ps = ps
        self.nrounds = nrounds
        self.m = m
        self.reqc = None
        self.waiting = set()
        self.replied = set()

    def _da_run_internal(self):

        def anounce():
            self._send(('cs in', self.logical_clock(), self.id), self.m)
            self._send(('cs out', self.logical_clock(), self.id), self.m)
        for i in range(self.nrounds):
            self.cs(anounce)
        self._send(('Done', self.id), self.ps)
        self._send(('Done', self.id), self.m)
        p = None

        def UniversalOpExpr_1():
            nonlocal p
            for p in self.ps:
                if (not PatternExpr_7.match_iter(self._PReceivedEvent_0, _BoundPattern33_=p)):
                    return False
            return True
        _st_label_49 = 0
        while (_st_label_49 == 0):
            _st_label_49 += 1
            if UniversalOpExpr_1():
                _st_label_49 += 1
            else:
                super()._label('_st_label_49', block=True)
                _st_label_49 -= 1

    def cs(self, task):
        super()._label('start', block=False)
        self.reqc = self.logical_clock()
        self._send(('Request', self.reqc), self.ps)
        _st_label_35 = 0
        while (_st_label_35 == 0):
            _st_label_35 += 1
            if (len(self.replied) == len(self.ps)):
                _st_label_35 += 1
            else:
                super()._label('_st_label_35', block=True)
                _st_label_35 -= 1
        task()
        super()._label('release', block=False)
        self.reqc = None
        self._send(('Reply', self.logical_clock()), self.waiting)
        super()._label('end', block=False)
        self.waiting = set()
        self.replied = set()

    def _P_handler_2(self, timestamp, source):
        if ((self.reqc == None) or ((timestamp, source) < (self.reqc, self.id))):
            self._send(('Reply', self.logical_clock()), source)
        else:
            self.waiting.add(source)
    _P_handler_2._labels = None
    _P_handler_2._notlabels = None

    def _P_handler_3(self, c1, source):
        if ((not (self.reqc is None)) and (c1 > self.reqc)):
            self.replied.add(source)
    _P_handler_3._labels = None
    _P_handler_3._notlabels = None

def main(p, r):
    nprocs = p
    nrounds = r
    da.config(channel=('fifo', 'reliable'), clock='Lamport')
    ps = da.new(P, num=nprocs)
    mtrs = da.new(M, num=1)
    for m in mtrs:
        da.setup(m, (ps, 5))
    da.start(mtrs)
    for p in ps:
        da.setup({p}, ((ps - {p}), nrounds, mtrs))
    da.start(ps)
