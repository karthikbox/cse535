
import da
PatternExpr_0 = da.pat.TuplePattern([da.pat.ConstantPattern('cs in'), da.pat.FreePattern('c2'), da.pat.FreePattern('p')])
PatternExpr_1 = da.pat.TuplePattern([da.pat.ConstantPattern('cs out'), da.pat.FreePattern('c2'), da.pat.FreePattern('p')])
PatternExpr_3 = da.pat.TuplePattern([da.pat.ConstantPattern('Done'), da.pat.BoundPattern('_BoundPattern10_')])
PatternExpr_4 = da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.FreePattern(None), da.pat.FreePattern(None)]), da.pat.TuplePattern([da.pat.ConstantPattern('Done'), da.pat.BoundPattern('_BoundPattern20_')])])
PatternExpr_5 = da.pat.TuplePattern([da.pat.ConstantPattern('access'), da.pat.FreePattern('newtok')])
PatternExpr_6 = da.pat.TuplePattern([da.pat.ConstantPattern('request'), da.pat.FreePattern('c'), da.pat.FreePattern('p')])
PatternExpr_7 = da.pat.TuplePattern([da.pat.ConstantPattern('request'), da.pat.FreePattern('c'), da.pat.BoundPattern('_BoundPattern30_')])
PatternExpr_9 = da.pat.TuplePattern([da.pat.ConstantPattern('access'), da.pat.FreePattern(None)])
PatternExpr_11 = da.pat.TuplePattern([da.pat.ConstantPattern('access'), da.pat.FreePattern('token1')])
PatternExpr_13 = da.pat.TuplePattern([da.pat.ConstantPattern('access'), da.pat.FreePattern('token2')])
PatternExpr_16 = da.pat.TuplePattern([da.pat.ConstantPattern('Done')])
PatternExpr_17 = da.pat.BoundPattern('_BoundPattern81_')
PatternExpr_18 = da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.FreePattern(None), da.pat.BoundPattern('_BoundPattern87_')]), da.pat.TuplePattern([da.pat.ConstantPattern('Done')])])
import sys
import time
from threading import Lock

class M(da.DistProcess):

    def __init__(self, parent, initq, channel, props):
        super().__init__(parent, initq, channel, props)
        self._MReceivedEvent_2 = []
        self._events.extend([da.pat.EventPattern(da.pat.ReceivedEvent, '_MReceivedEvent_0', PatternExpr_0, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._M_handler_0]), da.pat.EventPattern(da.pat.ReceivedEvent, '_MReceivedEvent_1', PatternExpr_1, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._M_handler_1]), da.pat.EventPattern(da.pat.ReceivedEvent, '_MReceivedEvent_2', PatternExpr_3, sources=None, destinations=None, timestamps=None, record_history=True, handlers=[])])

    def setup(self, s, nrequest):
        self.s = s
        self.nrequest = nrequest
        self.f = list()
        self.mutex = Lock()

    def _da_run_internal(self):
        p = None

        def UniversalOpExpr_0():
            nonlocal p
            for p in self.s:
                if (not PatternExpr_4.match_iter(self._MReceivedEvent_2, _BoundPattern20_=p)):
                    return False
            return True
        _st_label_26 = 0
        while (_st_label_26 == 0):
            _st_label_26 += 1
            if UniversalOpExpr_0():
                _st_label_26 += 1
            else:
                super()._label('_st_label_26', block=True)
                _st_label_26 -= 1
        self.solve()

    def solve(self):
        isSafe = True
        for i in range(1, len(self.f)):
            if (self.f[i][0] == 'cs out'):
                if ((not (self.f[(i - 1)][0] == 'cs in')) or (not (self.f[i][2] == self.f[(i - 1)][2]))):
                    isSafe = False
            elif (self.f[i][0] == 'cs in'):
                if ((not (self.f[(i - 1)][0] == 'cs out')) or (self.f[i][2] == self.f[(i - 1)][2])):
                    isSafe = False
        if isSafe:
            self.output('safety check PASSED')
        else:
            self.output('safety check FAILED')

    def _M_handler_0(self, c2, p):
        self.f.append(('cs in', c2, p))
    _M_handler_0._labels = None
    _M_handler_0._notlabels = None

    def _M_handler_1(self, c2, p):
        self.f.append(('cs out', c2, p))
    _M_handler_1._labels = None
    _M_handler_1._notlabels = None

class P(da.DistProcess):

    def __init__(self, parent, initq, channel, props):
        super().__init__(parent, initq, channel, props)
        self._PReceivedEvent_2 = []
        self._PSentEvent_3 = []
        self._PReceivedEvent_4 = []
        self._PSentEvent_5 = []
        self._PReceivedEvent_6 = []
        self._events.extend([da.pat.EventPattern(da.pat.ReceivedEvent, '_PReceivedEvent_0', PatternExpr_5, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._P_handler_2]), da.pat.EventPattern(da.pat.ReceivedEvent, '_PReceivedEvent_1', PatternExpr_6, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._P_handler_3]), da.pat.EventPattern(da.pat.ReceivedEvent, '_PReceivedEvent_2', PatternExpr_7, sources=None, destinations=None, timestamps=None, record_history=True, handlers=[]), da.pat.EventPattern(da.pat.SentEvent, '_PSentEvent_3', PatternExpr_9, sources=None, destinations=None, timestamps=None, record_history=True, handlers=[]), da.pat.EventPattern(da.pat.ReceivedEvent, '_PReceivedEvent_4', PatternExpr_11, sources=None, destinations=None, timestamps=None, record_history=True, handlers=[]), da.pat.EventPattern(da.pat.SentEvent, '_PSentEvent_5', PatternExpr_13, sources=None, destinations=None, timestamps=None, record_history=True, handlers=[]), da.pat.EventPattern(da.pat.ReceivedEvent, '_PReceivedEvent_6', PatternExpr_16, sources=[PatternExpr_17], destinations=None, timestamps=None, record_history=True, handlers=[])])

    def setup(self, ps, nrounds, orig_token, m):
        self.ps = ps
        self.nrounds = nrounds
        self.orig_token = orig_token
        self.m = m
        self.clock = 0
        self.token = dict(((p, 0) for p in self.ps))

    def _da_run_internal(self):

        def anounce():
            self._send(('cs in', 0, self.id), self.m)
            self._send(('cs out', 0, self.id), self.m)
        if self.token_present():
            pass
        for i in range(self.nrounds):
            self.cs(anounce)
        self._send(('Done',), self.ps)
        self._send(('Done', self.id), self.m)
        p = None

        def UniversalOpExpr_5():
            nonlocal p
            for p in self.ps:
                if (not PatternExpr_18.match_iter(self._PReceivedEvent_6, _BoundPattern87_=p)):
                    return False
            return True
        _st_label_62 = 0
        while (_st_label_62 == 0):
            _st_label_62 += 1
            if UniversalOpExpr_5():
                _st_label_62 += 1
            else:
                super()._label('_st_label_62', block=True)
                _st_label_62 -= 1

    def cs(self, task):
        super()._label('request', block=False)
        if (not self.token_present()):
            self.clock += 1
            self._send(('request', self.clock, self.id), self.ps)
            _st_label_36 = 0
            while (_st_label_36 == 0):
                _st_label_36 += 1
                if self.token_present():
                    _st_label_36 += 1
                else:
                    super()._label('_st_label_36', block=True)
                    _st_label_36 -= 1
        self.token[self.id] = self.clock
        task()
        super()._label('release', block=False)
        for p in self.ps:
            if (self.request_pending(p) and self.token_present()):
                self._send(('access', self.token), p)
                break

    def request_pending(self, p):
        c = None

        def ExistentialOpExpr_1():
            nonlocal c
            for (_, _, (_ConstantPattern39_, c, _BoundPattern41_)) in self._PReceivedEvent_2:
                if (_ConstantPattern39_ == 'request'):
                    if (_BoundPattern41_ == p):
                        if (c > self.token[p]):
                            return True
            return False
        return ExistentialOpExpr_1()

    def token_present(self):

        def ExistentialOpExpr_2():
            for (_, _, (_ConstantPattern52_, _)) in self._PSentEvent_3:
                if (_ConstantPattern52_ == 'access'):
                    if True:
                        return True
            return False
        token2 = token1 = None

        def ExistentialOpExpr_3():
            nonlocal token2, token1
            for (_, _, (_ConstantPattern64_, token1)) in self._PReceivedEvent_4:
                if (_ConstantPattern64_ == 'access'):

                    def ExistentialOpExpr_4(token1):
                        nonlocal token2
                        for (_, _, (_ConstantPattern76_, token2)) in self._PSentEvent_5:
                            if (_ConstantPattern76_ == 'access'):
                                if (token2[self.id] > token1[self.id]):
                                    return True
                        return False
                    if (not ExistentialOpExpr_4(token1=token1)):
                        return True
            return False
        return ((self.orig_token and (not ExistentialOpExpr_2())) or ExistentialOpExpr_3())

    def _P_handler_2(self, newtok):
        self.token = newtok
    _P_handler_2._labels = None
    _P_handler_2._notlabels = None

    def _P_handler_3(self, c, p):
        if (self.request_pending(p) and self.token_present()):
            self._send(('access', self.token), p)
    _P_handler_3._labels = None
    _P_handler_3._notlabels = None

def main(p, r):
    nprocs = p
    nrounds = r
    da.config(channel=('fifo', 'reliable'), clock='Lamport')
    ps = da.new(P, num=nprocs)
    mtrs = da.new(M, num=1)
    for m in mtrs:
        da.setup(m, (ps, 5))
    da.start(mtrs)
    time.sleep(1)
    p = ps.pop()
    da.setup(ps, ((ps | {p}), nrounds, False, mtrs))
    da.setup([p], ((ps | {p}), nrounds, True, mtrs))
    da.start((ps | {p}))
