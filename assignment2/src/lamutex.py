
import da
PatternExpr_0 = da.pat.TuplePattern([da.pat.ConstantPattern('cs in'), da.pat.FreePattern('c2'), da.pat.FreePattern('p')])
PatternExpr_1 = da.pat.TuplePattern([da.pat.ConstantPattern('cs out'), da.pat.FreePattern('c2'), da.pat.FreePattern('p')])
PatternExpr_3 = da.pat.TuplePattern([da.pat.ConstantPattern('done'), da.pat.BoundPattern('_BoundPattern10_')])
PatternExpr_4 = da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.FreePattern(None), da.pat.FreePattern(None)]), da.pat.TuplePattern([da.pat.ConstantPattern('done'), da.pat.BoundPattern('_BoundPattern20_')])])
PatternExpr_7 = da.pat.TuplePattern([da.pat.ConstantPattern('ack'), da.pat.FreePattern('c2'), da.pat.BoundPattern('_BoundPattern28_')])
PatternExpr_9 = da.pat.TuplePattern([da.pat.ConstantPattern('request'), da.pat.FreePattern('c2'), da.pat.FreePattern('p')])
PatternExpr_10 = da.pat.TuplePattern([da.pat.ConstantPattern('release'), da.pat.FreePattern(None), da.pat.FreePattern('p')])
PatternExpr_13 = da.pat.TuplePattern([da.pat.ConstantPattern('done'), da.pat.BoundPattern('_BoundPattern54_')])
PatternExpr_14 = da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.TuplePattern([da.pat.FreePattern(None), da.pat.FreePattern(None), da.pat.FreePattern(None)]), da.pat.TuplePattern([da.pat.ConstantPattern('done'), da.pat.BoundPattern('_BoundPattern64_')])])
import sys
import time

class M(da.DistProcess):

    def __init__(self, parent, initq, channel, props):
        super().__init__(parent, initq, channel, props)
        self._MReceivedEvent_2 = []
        self._events.extend([da.pat.EventPattern(da.pat.ReceivedEvent, '_MReceivedEvent_0', PatternExpr_0, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._M_handler_0]), da.pat.EventPattern(da.pat.ReceivedEvent, '_MReceivedEvent_1', PatternExpr_1, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._M_handler_1]), da.pat.EventPattern(da.pat.ReceivedEvent, '_MReceivedEvent_2', PatternExpr_3, sources=None, destinations=None, timestamps=None, record_history=True, handlers=[])])

    def setup(self, s, nrequest):
        self.s = s
        self.nrequest = nrequest
        self.f = list()

    def _da_run_internal(self):
        p = None

        def UniversalOpExpr_0():
            nonlocal p
            for p in self.s:
                if (not PatternExpr_4.match_iter(self._MReceivedEvent_2, _BoundPattern20_=p)):
                    return False
            return True
        _st_label_26 = 0
        while (_st_label_26 == 0):
            _st_label_26 += 1
            if UniversalOpExpr_0():
                _st_label_26 += 1
            else:
                super()._label('_st_label_26', block=True)
                _st_label_26 -= 1
        self.solve()

    def solve(self):
        isSafe = True
        for i in range(1, len(self.f)):
            if (self.f[i][0] == 'cs out'):
                if ((not (self.f[(i - 1)][0] == 'cs in')) or (not (self.f[i][2] == self.f[(i - 1)][2]))):
                    isSafe = False
                    break
            elif (self.f[i][0] == 'cs in'):
                if ((not (self.f[(i - 1)][0] == 'cs out')) or (self.f[i][2] == self.f[(i - 1)][2])):
                    isSafe = False
                    break
        if isSafe:
            self.output('safety check PASSED')
        else:
            self.output('safety check FAILED')

    def _M_handler_0(self, c2, p):
        self.f.append(('cs in', c2, p))
    _M_handler_0._labels = None
    _M_handler_0._notlabels = None

    def _M_handler_1(self, c2, p):
        self.f.append(('cs out', c2, p))
    _M_handler_1._labels = None
    _M_handler_1._notlabels = None

class P(da.DistProcess):

    def __init__(self, parent, initq, channel, props):
        super().__init__(parent, initq, channel, props)
        self._PReceivedEvent_0 = []
        self._PReceivedEvent_3 = []
        self._events.extend([da.pat.EventPattern(da.pat.ReceivedEvent, '_PReceivedEvent_0', PatternExpr_7, sources=None, destinations=None, timestamps=None, record_history=True, handlers=[]), da.pat.EventPattern(da.pat.ReceivedEvent, '_PReceivedEvent_1', PatternExpr_9, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._P_handler_2]), da.pat.EventPattern(da.pat.ReceivedEvent, '_PReceivedEvent_2', PatternExpr_10, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._P_handler_3]), da.pat.EventPattern(da.pat.ReceivedEvent, '_PReceivedEvent_3', PatternExpr_13, sources=None, destinations=None, timestamps=None, record_history=True, handlers=[])])

    def setup(self, s, nrequests, m):
        self.s = s
        self.nrequests = nrequests
        self.m = m
        self.q = set()

    def _da_run_internal(self):

        def task():
            self._send(('cs in', self.logical_clock(), self.id), self.m)
            self._send(('cs out', self.logical_clock(), self.id), self.m)
        for i in range(self.nrequests):
            self.mutex(task)
        self._send(('done', self.id), self.s)
        self._send(('done', self.id), self.m)
        p = None

        def UniversalOpExpr_4():
            nonlocal p
            for p in self.s:
                if (not PatternExpr_14.match_iter(self._PReceivedEvent_3, _BoundPattern64_=p)):
                    return False
            return True
        _st_label_54 = 0
        while (_st_label_54 == 0):
            _st_label_54 += 1
            if UniversalOpExpr_4():
                _st_label_54 += 1
            else:
                super()._label('_st_label_54', block=True)
                _st_label_54 -= 1

    def mutex(self, task):
        super()._label('request', block=False)
        c = self.logical_clock()
        self._send(('request', c, self.id), self.s)
        self.q.add(('request', c, self.id))
        p = c2 = None

        def UniversalOpExpr_1():
            nonlocal p, c2
            for (_ConstantPattern21_, c2, p) in self.q:
                if (_ConstantPattern21_ == 'request'):
                    if (not (((c2, p) == (c, self.id)) or ((c, self.id) < (c2, p)))):
                        return False
            return True
        p = c2 = None

        def UniversalOpExpr_2():
            nonlocal p, c2
            for p in self.s:

                def ExistentialOpExpr_3(p):
                    nonlocal c2
                    for (_, _, (_ConstantPattern37_, c2, _BoundPattern39_)) in self._PReceivedEvent_0:
                        if (_ConstantPattern37_ == 'ack'):
                            if (_BoundPattern39_ == p):
                                if (c2 > c):
                                    return True
                    return False
                if (not ExistentialOpExpr_3(p=p)):
                    return False
            return True
        _st_label_35 = 0
        while (_st_label_35 == 0):
            _st_label_35 += 1
            if (UniversalOpExpr_1() and UniversalOpExpr_2()):
                _st_label_35 += 1
            else:
                super()._label('_st_label_35', block=True)
                _st_label_35 -= 1
        super()._label('critical_section', block=False)
        task()
        super()._label('release', block=False)
        self.q.remove(('request', c, self.id))
        self._send(('release', self.logical_clock(), self.id), self.s)

    def _P_handler_2(self, c2, p):
        self.q.add(('request', c2, p))
        self._send(('ack', self.logical_clock(), self.id), p)
    _P_handler_2._labels = None
    _P_handler_2._notlabels = None

    def _P_handler_3(self, p):
        for x in {('request', c, p) for (_ConstantPattern48_, c, _BoundPattern50_) in self.q if (_ConstantPattern48_ == 'request') if (_BoundPattern50_ == p)}:
            self.q.remove(x)
            break
    _P_handler_3._labels = None
    _P_handler_3._notlabels = None

def main(p, r):
    nprocs = p
    nrequests = r
    da.config(channel=('fifo', 'reliable'), clock='Lamport')
    ps = da.new(P, num=nprocs)
    mtrs = da.new(M, num=1)
    for m in mtrs:
        da.setup(m, (ps, 5))
    da.start(mtrs)
    for p in ps:
        da.setup(p, ((ps - {p}), nrequests, mtrs))
    da.start(ps)
